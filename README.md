# My Zing MP3 cho bài thi GK

- Ứng dụng gồm màn hình đầu tiên là đăng ký đăng nhập. Bạn có thể tạo một tài khoản mới bằng cách nhấn vào đăng ký và sau khi đăng ký xong bạn đã có tài khoản của mình trong hệ thống dành cho những lần đăng nhập sau(Tài khoản mẫu: tuan310801anh@gmail.com | MK: 1234567890)

- Giao diện màn hình chính
	+ Đầu tiên là banner quảng cáo những bài hát mới ra trên app, bạn bấm vào sẽ thấy màn hình gồm bài hát mới đó và sau khi bấm vào sẽ tới màn hình phát nhạc.
	+ Tiếp đến là ALBUM HOT bạn sẽ thấy danh sách gồm 5 album hot, điều đặc biệt là 5 album này sẽ không cố định mà thay đổi theo ngày. Bạn có thể bấm vào "xem thêm" để hiện ra màn hình toàn bộ album. Sau khi bấm vào album, màn hình sẽ hiện ra danh sách các bài hát trong album đó và bạn bấm vào 1 bài hát bất kỳ thì giao diện đĩa nhạc sẽ hiện ra.
	+ Thứ ba là Playlist hôm nay, cũng tương tự như Album hot, 3 playlist sẽ thay đổi theo ngày. Bạn bấm vào "XEM TẤY CẢ PLAYLIST" để xem toàn bộ playlist. Sau khi bấm vào playlist, màn hình sẽ hiện ra danh sách các bài hát trong playlist đó và bạn bấm vào 1 bài hát bất kỳ thì giao diện đĩa nhạc sẽ hiện ra.
	+ 1 phần không thể thiếu tiếp theo là "Chủ đề mà bạn sẽ thích", các chủ đề sẽ thay đổi theo ngày. Bấm vào "Xem thêm" các bạn sẽ thấy toàn bộ các chủ đề và sau khi bấm vào từng chủ đề nó sẽ hiện ra những thể loại tương ứng với chủ đề đó. Sau khi bấm vào thể loại, màn hình sẽ hiện ra danh sách các bài hát trong thể loại đó và bạn bấm vào 1 bài hát bất kỳ thì giao diện đĩa nhạc sẽ hiện ra.
	+ Cuối cùng là TOP TRENDING gồm những bài nhạc được yêu thích nhất. Bấm vào bài hát đó màn hình đĩa nhạc sẽ hiện ra
