import '../models/baihat.dart';
import 'package:http/http.dart' as http;

Future<List<Baihat>> fetchBaiHat() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/baihatduocthich.php"));
  return baihatFromJson(response.body);
}

Future<List<Baihat>> fetchBaiHatTheoAlbum(String idalbum) async{
  final response = await http.post(
      Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/danhsachbaihat.php"),
    body: {
      "idalbum" : idalbum
    }
  );
  return baihatFromJson(response.body);
}

Future<List<Baihat>> fetchBaiHatTheoPlaylist(String idplaylist) async{
  final response = await http.post(
      Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/danhsachbaihat.php"),
      body: {
        "idplaylist" : idplaylist
      }
  );
  return baihatFromJson(response.body);
}

Future<List<Baihat>> fetchBaiHatTheoTheloai(String idtheloai) async{
  final response = await http.post(
      Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/danhsachbaihat.php"),
      body: {
        "idtheloai" : idtheloai
      }
  );
  return baihatFromJson(response.body);
}

Future<List<Baihat>> fetchBaiHatTheoQuangcao(String idquangcao) async{
  final response = await http.post(
      Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/danhsachbaihat.php"),
      body: {
        "idquangcao" : idquangcao
      }
  );
  return baihatFromJson(response.body);
}