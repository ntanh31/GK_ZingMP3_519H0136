import '../models/chude.dart';
import 'package:http/http.dart' as http;

Future<List<Chude>> fetchChuDe() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/chudetrongngay.php"));
  return chudeFromJson(response.body);
}

Future<List<Chude>> fullChuDe() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/tatcachude.php"));
  return chudeFromJson(response.body);
}
