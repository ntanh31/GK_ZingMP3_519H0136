import '../models/quangcao.dart';
import 'package:http/http.dart' as http;

Future<List<Quangcao>> fetchQuangCao() async{
  final response = await http.get(Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/songbanner.php"));
  return quangcaoFromJson(response.body);
}

