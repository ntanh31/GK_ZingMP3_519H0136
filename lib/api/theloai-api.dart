import '../models/theloai.dart';
import 'package:http/http.dart' as http;

Future<List<Theloai>> fetchTheLoaiTheoChuDe(String idchude) async{
  final response = await http.post(
      Uri.parse("https://anhnguyendb.000webhostapp.com/public_html/Server/theloaitheochude.php"),
      body: {
        "idchude" : idchude
      }
  );
  return theloaiFromJson(response.body);
}