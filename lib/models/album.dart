import 'dart:convert';

List<Album> albumFromJson(String str) => List<Album>.from(json.decode(str).map((x) => Album.fromJson(x)));

String albumToJson(List<Album> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Album {
  String? idAlbum;
  String? tenAlbum;
  String? tenCaSiAlbum;
  String? hinhAlbum;

  Album({
    this.idAlbum,
    this.tenAlbum,
    this.tenCaSiAlbum,
    this.hinhAlbum,
  });

  factory Album.fromJson(Map<String, dynamic> json) => Album(
    idAlbum: json["IdAlbum"],
    tenAlbum: json["TenAlbum"],
    tenCaSiAlbum: json["TenCaSiAlbum"],
    hinhAlbum: json["HinhAlbum"],
  );

  Map<String, dynamic> toJson() => {
    "IdAlbum": idAlbum,
    "TenAlbum": tenAlbum,
    "TenCaSiAlbum": tenCaSiAlbum,
    "HinhAlbum": hinhAlbum,
  };
}