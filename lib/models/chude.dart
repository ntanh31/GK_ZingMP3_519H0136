import 'dart:convert';

List<Chude> chudeFromJson(String str) => List<Chude>.from(json.decode(str).map((x) => Chude.fromJson(x)));

String chudeToJson(List<Chude> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Chude {
  String? idChuDe;
  String? tenChuDe;
  String? hinhChuDe;

  Chude({
    this.idChuDe,
    this.tenChuDe,
    this.hinhChuDe,
  });


  factory Chude.fromJson(Map<String, dynamic> json) => Chude(
    idChuDe: json["IdChuDe"],
    tenChuDe: json["TenChuDe"],
    hinhChuDe: json["HinhChuDe"],
  );

  Map<String, dynamic> toJson() => {
    "IdChuDe": idChuDe,
    "TenChuDe": tenChuDe,
    "HinhChuDe": hinhChuDe,
  };
}
