import 'dart:convert';

List<Playlist> playlistFromJson(String str) => List<Playlist>.from(json.decode(str).map((x) => Playlist.fromJson(x)));

String playlistToJson(List<Playlist> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Playlist {
  String? idPlaylist;
  String? ten;
  String? hinhPlaylist;
  String? icon;

  Playlist({
    this.idPlaylist,
    this.ten,
    this.hinhPlaylist,
    this.icon,
  });

  factory Playlist.fromJson(Map<String, dynamic> json) => Playlist(
    idPlaylist: json["IdPlaylist"],
    ten: json["Ten"],
    hinhPlaylist: json["HinhPlaylist"],
    icon: json["Icon"],
  );

  Map<String, dynamic> toJson() => {
    "IdPlaylist": idPlaylist,
    "Ten": ten,
    "HinhPlaylist": hinhPlaylist,
    "Icon": icon,
  };
}
