// To parse this JSON data, do
//
//     final quangcao = quangcaoFromJson(jsonString);

import 'dart:convert';

List<Quangcao> quangcaoFromJson(String str) => List<Quangcao>.from(json.decode(str).map((x) => Quangcao.fromJson(x)));

String quangcaoToJson(List<Quangcao> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Quangcao {
  String? idQuangCao;
  String? hinhanh;
  String? noidung;
  String? idBaiHat;
  String? tenBaiHat;
  String? hinhBaiHat;

  Quangcao({
    this.idQuangCao,
    this.hinhanh,
    this.noidung,
    this.idBaiHat,
    this.tenBaiHat,
    this.hinhBaiHat,
  });

  factory Quangcao.fromJson(Map<String, dynamic> json) => Quangcao(
    idQuangCao: json["IdQuangCao"],
    hinhanh: json["Hinhanh"],
    noidung: json["Noidung"],
    idBaiHat: json["IdBaiHat"],
    tenBaiHat: json["TenBaiHat"],
    hinhBaiHat: json["HinhBaiHat"],
  );

  Map<String, dynamic> toJson() => {
    "IdQuangCao": idQuangCao,
    "Hinhanh": hinhanh,
    "Noidung": noidung,
    "IdBaiHat": idBaiHat,
    "TenBaiHat": tenBaiHat,
    "HinhBaiHat": hinhBaiHat,
  };
}
