import 'dart:convert';

List<Theloai> theloaiFromJson(String str) => List<Theloai>.from(json.decode(str).map((x) => Theloai.fromJson(x)));

String theloaiToJson(List<Theloai> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Theloai {
  String? idTheLoai;
  String? idKeyChuDe;
  String? tenTheLoai;
  String? hinhTheLoai;
  
  Theloai({
    this.idTheLoai,
    this.idKeyChuDe,
    this.tenTheLoai,
    this.hinhTheLoai,
  });

  factory Theloai.fromJson(Map<String, dynamic> json) => Theloai(
    idTheLoai: json["IdTheLoai"],
    idKeyChuDe: json["IdKeyChuDe"],
    tenTheLoai: json["TenTheLoai"],
    hinhTheLoai: json["HinhTheLoai"],
  );

  Map<String, dynamic> toJson() => {
    "IdTheLoai": idTheLoai,
    "IdKeyChuDe": idKeyChuDe,
    "TenTheLoai": tenTheLoai,
    "HinhTheLoai": hinhTheLoai,
  };
}