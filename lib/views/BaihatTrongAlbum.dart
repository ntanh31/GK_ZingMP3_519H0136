import 'package:flutter/material.dart';
import '../api/baihat-api.dart';
import '../models/album.dart';
import '../models/baihat.dart';
import 'MusicPlayer.dart';

class BaihatTrongAlbum extends StatelessWidget {
  Album? album;
  BaihatTrongAlbum(this.album);

  @override
  Widget build(BuildContext context) {
    Map<String, Album>? arguments = ModalRoute.of(context)!.settings.arguments as Map<String, Album>?;
    album = arguments!['album'];
    Color mainColor = Color(0xff7200a1);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("${album!.tenAlbum}"),
          backgroundColor: mainColor,
        ),
        backgroundColor: mainColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Image.network(
                album?.hinhAlbum as String,
              ),
              SizedBox(height: 10,),
              Text("DANH SÁCH BÀI HÁT", style: TextStyle(fontSize: 25, color: Colors.white),),
              SizedBox(height: 30,),
              Container(
                child: FutureBuilder(
                  future: fetchBaiHatTheoAlbum(album!.idAlbum as String),
                  builder: (context, AsyncSnapshot snapshot){
                    if(snapshot.hasData){
                      return Container(
                        height: 1000,
                        child: ListView.builder(
                          physics: PageScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, index){
                            Baihat baihat = snapshot.data[index];
                            return Card(
                              elevation: 0,
                              color: Colors.transparent,
                              child: InkWell(
                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => MusicPlayer(baihat)));
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Image.network(
                                        baihat.hinhBaiHat!,
                                        height: 120,
                                        width: 120,
                                      ),
                                      SizedBox(width: 10,),
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text("${baihat.tenBaiHat}",overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 20, color: Colors.white)),
                                          Text("${baihat.caSi}", style: TextStyle(fontSize: 17, color: Colors.white)),
                                        ],
                                      )
                                    ],
                                  )
                              ),
                            );
                          },
                        ),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
