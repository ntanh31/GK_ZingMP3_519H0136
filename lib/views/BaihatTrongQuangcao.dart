import 'package:flutter/material.dart';
import '../api/baihat-api.dart';
import '../models/quangcao.dart';
import '../models/baihat.dart';
import 'MusicPlayer.dart';

class BaihatTrongQuangcao extends StatelessWidget {
  Quangcao? quangcao;
  BaihatTrongQuangcao(this.quangcao);


  @override
  Widget build(BuildContext context) {
    Map<String, Quangcao>? arguments = ModalRoute.of(context)!.settings.arguments as Map<String, Quangcao>?;
    quangcao = arguments!['quangcao'];
    Color mainColor = Color(0xff7200a1);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("${quangcao!.tenBaiHat}"),
          backgroundColor: mainColor,
        ),
        backgroundColor: mainColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Image.network(
                quangcao!.hinhanh as String,
              ),
              SizedBox(height: 10,),
              Text("DANH SÁCH BÀI HÁT", style: TextStyle(fontSize: 25, color: Colors.white),),
              SizedBox(height: 30,),
              Container(
                child: FutureBuilder(
                  future: fetchBaiHatTheoQuangcao(quangcao!.idQuangCao as String),
                  builder: (context, AsyncSnapshot snapshot){
                    if(snapshot.hasData){
                      return Container(
                        height: 500,
                        child: ListView.builder(
                          physics: PageScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, index){
                            Baihat baihat = snapshot.data[index];
                            return Card(
                              elevation: 0,
                              color: Colors.transparent,
                              child: InkWell(
                                  onTap: (){
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => MusicPlayer(baihat)));
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Image.network(
                                        baihat.hinhBaiHat!,
                                        height: 120,
                                        width: 120,
                                      ),
                                      SizedBox(width: 10,),
                                      Expanded(
                                        child: Container(
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text("${baihat.tenBaiHat}",overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 20, color: Colors.white)),
                                                Text("${baihat.caSi}", style: TextStyle(fontSize: 17, color: Colors.white)),
                                              ],
                                            )
                                        ),
                                      )
                                    ],
                                  )
                              ),
                            );
                          },
                        ),
                      );
                    }
                    return CircularProgressIndicator();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
