import 'package:flutter/material.dart';
import '../api/playlist-api.dart';
import '../models/playlist.dart';

class FullPlaylist extends StatefulWidget {
  const FullPlaylist({Key? key}) : super(key: key);

  @override
  State<FullPlaylist> createState() => _FullPlaylistState();
}

class _FullPlaylistState extends State<FullPlaylist> {
  @override
  Widget build(BuildContext context) {
    Color mainColor = Color(0xff7200a1);
    return SafeArea(
      child: Scaffold(
        backgroundColor: mainColor,
        appBar: AppBar(
          backgroundColor: mainColor,
          title: Text("Tất cả Playlist"),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.pushReplacementNamed(context, "home"),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: FutureBuilder(
              future: fullPlaylist(),
              builder: (context, AsyncSnapshot snapshot){
                if(snapshot.hasData){
                  return Container(
                    height: 1800,
                    child: ListView.builder(
                      physics: PageScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, index){
                        Playlist playlist = snapshot.data[index];
                        return Card(
                          child: InkWell(
                            onTap: (){
                              Navigator.pushNamed(context, 'baihattrongPlaylist', arguments: {'playlist': playlist});
                            },
                            child: Column(
                              children: [
                                Image.network(
                                  playlist.hinhPlaylist!,
                                ),
                                Text("${playlist.ten}", style: TextStyle(fontSize: 20,),),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
                return CircularProgressIndicator();
              },
            ),
          ),
        ),
      ),
    );
  }
}
