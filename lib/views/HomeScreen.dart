import 'package:flutter/material.dart';
import '../views/InfoScreen.dart';
import '../views/MainScreen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  final screens = [
    MainScreen(),
    InfoScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: IndexedStack(
            index: currentIndex,
            children: screens,
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            iconSize: 35,
            backgroundColor: Colors.white,
            selectedItemColor: Colors.blue,
            onTap: (index){
              setState(() {
                currentIndex = index;
              });
            },
            currentIndex: currentIndex,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.home_outlined),
                  label:  "Trang Chủ",
                  backgroundColor: Colors.blue
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.person_outline_rounded),
                  label:  "Cá Nhân",
                  backgroundColor: Colors.blue
              ),
            ],
          ),
        )
    );
  }
}
