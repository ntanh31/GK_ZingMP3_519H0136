import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../api/album-api.dart';
import '../api/baihat-api.dart';
import '../api/chude-api.dart';
import '../api/playlist-api.dart';
import '../api/quangcao-api.dart';
import '../models/album.dart';
import '../models/quangcao.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../models/playlist.dart';
import '../views/MusicPlayer.dart';
import '../models/baihat.dart';
import '../models/chude.dart';


class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    Color mainColor = Color(0xff7200a1);
    return SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          body: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: FutureBuilder(
                      future: fetchQuangCao(),
                      builder: (context, AsyncSnapshot snapshot){
                        if(snapshot.hasData){
                          return CarouselSlider.builder(
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, int index, _){
                                Quangcao quangcao = snapshot.data[index];
                                return Card(
                                  elevation: 0,
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: (){
                                      Navigator.pushNamed(context, 'baihattrongQuangcao', arguments: {'quangcao': quangcao});
                                    },
                                    child: Column(
                                      children: [
                                        Image.network(
                                          quangcao.hinhanh!,
                                        ),
                                        Text("${quangcao.noidung}", style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Colors.white),),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              options: CarouselOptions(
                                initialPage: 3,
                                autoPlay: true,
                              )
                          );
                        }
                        return CircularProgressIndicator();
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Text("ALBUM HOT", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                      SizedBox(width: 180,),
                      GestureDetector(
                        child: Text('xem thêm', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                        onTap: (){
                          Navigator.pushReplacementNamed(context, "fullAlbum");
                        },
                      ),
                    ],
                  ),
                  Container(
                    child: FutureBuilder(
                      future: fetchAlbum(),
                      builder: (context, AsyncSnapshot snapshot){
                        if(snapshot.hasData){
                          return Container(
                            height: 200,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, index){
                                Album album = snapshot.data[index];
                                return Card(
                                  elevation: 0,
                                  color: Colors.transparent,
                                  child: InkWell(
                                    onTap: (){
                                      Navigator.pushNamed(context, 'baihattrongAlbum', arguments: {'album': album});
                                    },
                                    child: Column(
                                      children: [
                                        Image.network(
                                            album.hinhAlbum!,
                                          width: 150,
                                          height: 150,
                                        ),
                                        Text("${album.tenAlbum}", style: TextStyle(color: Colors.white), overflow: TextOverflow.ellipsis,),
                                        Text("${album.tenCaSiAlbum}",style: TextStyle(color: Colors.white)),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            )
                          );
                        }
                        return CircularProgressIndicator();
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Center(
                    child: Text("PLAYLIST HÔM NAY!", style: TextStyle(fontSize: 30, color: Colors.white),),
                  ),
                  Container(
                    child: FutureBuilder(
                      future: fetchPlaylist(),
                      builder: (context, AsyncSnapshot snapshot){
                        if(snapshot.hasData){
                          return Container(
                              height: 750,
                              child: ListView.builder(
                                physics: PageScrollPhysics(),
                                scrollDirection: Axis.vertical,
                                itemCount: snapshot.data.length,
                                itemBuilder: (BuildContext context, index){
                                  Playlist playlist = snapshot.data[index];
                                  return Card(
                                    child: InkWell(
                                      onTap: (){
                                        Navigator.pushNamed(context, 'baihattrongPlaylist', arguments: {'playlist': playlist});
                                        },
                                      child: Column(
                                        children: [
                                          Image.network(
                                            playlist.hinhPlaylist!,
                                          ),
                                          Text("${playlist.ten}", style: TextStyle(fontSize: 20,),),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              ),
                          );
                        }
                        return CircularProgressIndicator();
                      },
                    ),
                  ),
                  Container(
                    color: Colors.transparent,
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(left: 5, right: 5),
                    child: GestureDetector(
                      child: Text('Xem tất cả playlist', style: TextStyle(fontSize: 20,color: Colors.white, fontWeight: FontWeight.bold),),
                      onTap: (){
                        Navigator.pushReplacementNamed(context, "fullPlaylist");
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      Text("Chủ đề mà bạn sẽ thích", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                      SizedBox(width: 85,),
                      GestureDetector(
                        child: Text('xem thêm', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                        onTap: (){
                          Navigator.pushReplacementNamed(context, "fullChude");
                        },
                      ),
                    ],
                  ),
                  Container(
                    child: FutureBuilder(
                      future: fetchChuDe(),
                      builder: (context, AsyncSnapshot snapshot){
                        if(snapshot.hasData){
                          return Container(
                              height: 127,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: snapshot.data.length,
                                itemBuilder: (BuildContext context, index){
                                  Chude chude = snapshot.data[index];
                                  return Card(
                                    child: InkWell(
                                      onTap: (){
                                        Navigator.pushNamed(context, 'theloaitheoChude', arguments: {'chude': chude});
                                      },
                                      child: Column(
                                        children: [
                                          Image.network(
                                            chude.hinhChuDe!,
                                            width: 410,
                                            height: 119,
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              )
                          );
                        }
                        return CircularProgressIndicator();
                      },
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Text("TOP TRENDING", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
                  Container(
                    child: FutureBuilder(
                      future: fetchBaiHat(),
                      builder: (context, AsyncSnapshot snapshot){
                        if(snapshot.hasData){
                          return Container(
                            height: 650,
                            child: ListView.builder(
                              physics: PageScrollPhysics(),
                              scrollDirection: Axis.vertical,
                              itemCount: snapshot.data.length,
                              itemBuilder: (BuildContext context, index){
                                Baihat baihat = snapshot.data[index];
                                return Card(
                                  elevation: 0,
                                  color: Colors.transparent,
                                  child: InkWell(
                                      onTap: (){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => MusicPlayer(baihat)));
                                      },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Image.network(
                                            baihat.hinhBaiHat!,
                                          height: 120,
                                          width: 120,
                                        ),
                                        SizedBox(width: 10,),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("${baihat.tenBaiHat}", style: TextStyle(fontSize: 20, color: Colors.white),),
                                            Text("${baihat.caSi}", style: TextStyle(fontSize: 17, color: Colors.white),)
                                          ],
                                        )
                                      ],
                                    )
                                  ),
                                );
                              },
                            ),
                          );
                        }
                        return CircularProgressIndicator();
                      },
                    ),
                  ),
                ],
              )
          ),
        )
    );
  }
}
