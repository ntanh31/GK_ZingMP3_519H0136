import 'package:flutter/material.dart';
import '../api/theloai-api.dart';
import '../models/chude.dart';
import '../models/theloai.dart';

class TheloaiTheoChuDe extends StatelessWidget {
  Chude? chude;
  TheloaiTheoChuDe(chude);

  @override
  Widget build(BuildContext context) {
    Map<String, Chude>? arguments = ModalRoute.of(context)!.settings.arguments as Map<String, Chude>?;
    chude = arguments!['chude'];

    Color mainColor = Color(0xff7200a1);
    return SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          appBar: AppBar(
            backgroundColor: mainColor,
            title: Text("${chude!.tenChuDe}"),
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.pushReplacementNamed(context, "home"),
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              child: FutureBuilder(
                future: fetchTheLoaiTheoChuDe(chude!.idChuDe as String),
                builder: (context, AsyncSnapshot snapshot){
                  if(snapshot.hasData){
                    return Container(
                        height: 1500,
                        child: GridView.builder(
                          physics: PageScrollPhysics(),
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:2),
                          scrollDirection: Axis.vertical,
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, index){
                            Theloai theloai = snapshot.data[index];
                            return Card(
                              elevation: 0,
                              color: Colors.transparent,
                              child: InkWell(
                                onTap: (){
                                  Navigator.pushNamed(context, 'baihattrongTheloai', arguments: {'theloai': theloai});
                                },
                                child: Column(
                                  children: [
                                    Image.network(
                                      theloai.hinhTheLoai!,
                                      width: 150,
                                      height: 150,
                                    ),
                                    Text("${theloai.tenTheLoai}", style: TextStyle(fontSize: 17, color: Colors.white) ,overflow: TextOverflow.ellipsis,),
                                    Text("Nhiều nghệ sĩ", style: TextStyle(fontSize: 15, color: Colors.white)),
                                  ],
                                ),
                              ),
                            );
                          },
                        )
                    );
                  }
                  return CircularProgressIndicator();
                },
              ),
            ),
          ),
        ));
  }
}
