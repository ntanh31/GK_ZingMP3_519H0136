import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<UserCredential> googleSignIn() async {
    GoogleSignIn googleSignIn = GoogleSignIn();
    GoogleSignInAccount? googleUser = await googleSignIn.signIn();
    if (googleUser != null) {
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;

      if (googleAuth.idToken != null && googleAuth.accessToken != null) {
        final AuthCredential credential = GoogleAuthProvider.credential(
            accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);

        final UserCredential user =
        await _auth.signInWithCredential(credential);

        await Navigator.pushReplacementNamed(context, "/");

        return user;
      } else {
        throw StateError('Missing Google Auth Token');
      }
    } else
      throw StateError('Sign in Aborted');
  }

  navigateToLogin() async {
    Navigator.pushReplacementNamed(context, "Login");
  }

  navigateToRegister() async {
    Navigator.pushReplacementNamed(context, "SignUp");
  }

  @override
  Widget build(BuildContext context) {
    Color mainColor = Color(0xff7200a1);

    return SafeArea(
        child: Scaffold(
          backgroundColor: mainColor,
          body: Center(
            child: Column(
              children: [
                SizedBox(height: 20,),
                Container(
                  child: const Image(
                    image: AssetImage("images/logo.png"),
                    width: 250,
                    height: 250,
                  ),
                ),
                Container(
                  child: const Image(image: AssetImage("images/logo2.png")),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RaisedButton(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        onPressed: navigateToLogin,
                        child: const Text(
                          'ĐĂNG NHẬP',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.orange),
                    SizedBox(width: 20.0),
                    RaisedButton(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        onPressed: navigateToRegister,
                        child: const Text(
                          'ĐĂNG KÝ',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        color: Colors.orange),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                FloatingActionButton.extended(
                    onPressed: googleSignIn,
                    icon: Image.asset(
                      "images/google.png",
                      height: 30,
                      width: 30,
                    ),
                    label: Text("Đăng nhập với GOOGLE"),
                  backgroundColor: Colors.white,
                  foregroundColor: Colors.black,
                ),
              ],
            ),
          ),
        )
    );
  }
}
